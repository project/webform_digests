<?php

namespace Drupal\webform_digests;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\webform\Entity\Webform;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a listing of Webform digest entities.
 */
class WebformDigestListBuilder extends ConfigEntityListBuilder {

  /**
   * The messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Constructs a WebformDigestListBuilder object.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type definition.
   * @param \Drupal\Core\Entity\EntityStorageInterface $storage
   *   The entity storage class.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   */
  public function __construct(EntityTypeInterface $entity_type, EntityStorageInterface $storage, MessengerInterface $messenger) {
    parent::__construct($entity_type, $storage);
    $this->messenger = $messenger;
  }

  /**
   * {@inheritDoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    return new static(
      $entity_type,
      $container->get('entity_type.manager')->getStorage($entity_type->id()),
      $container->get('messenger')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['label'] = $this->t('Webform digest');
    $header['id'] = $this->t('Machine name');
    $header['Webform'] = $this->t('webform');
    $header['conditional'] = $this->t('Conditional');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    if (!$webform = Webform::load($entity->getWebform())) {
      $this->messenger->addWarning(t('%webform depends on a removed webform', ['%webform' => $entity->label()]));
    }

    $row['label'] = $entity->label();
    $row['id'] = $entity->id();
    $row['webform'] = $webform ? $webform->label() : '';
    $row['conditional'] = $entity->isConditional() ? t("Conditional") : t("All submissions");
    return $row + parent::buildRow($entity);
  }

}
