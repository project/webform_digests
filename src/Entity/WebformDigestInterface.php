<?php

namespace Drupal\webform_digests\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Webform digest entities.
 */
interface WebformDigestInterface extends ConfigEntityInterface {

}
