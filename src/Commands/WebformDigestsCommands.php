<?php

namespace Drupal\webform_digests\Commands;

use Drush\Commands\DrushCommands;
use Drupal\webform_digests\WebformDigestsQueueBuilder;

/**
 * Class WebformDigestsCommands
 *
 * @package Drupal\webform_digests\Commands
 */
class WebformDigestsCommands extends DrushCommands
{

  /**
   * The webform digests queue builder.
   *
   * @var \Drupal\webform_digests\WebformDigestsQueueBuilder
   */
  protected $webformDigestsQueueBuilder;

  /**
   * Constructs a new WebformDigestsCommands object.
   *
   * @param \Drupal\webform_digests\WebformDigestsQueueBuilder $webformDigestsQueueBuilder
   *   The webform digests queue builder.
   */
  public function __construct(WebformDigestsQueueBuilder $webformDigestsQueueBuilder) {
    $this->webformDigestsQueueBuilder = $webformDigestsQueueBuilder;
  }

  /**
   * Queue webform digests to be sent.
   *
   * @command webform:queue-digests
   */
  public function queueDigests(): void
  {
    $queue_count = $this->webformDigestsQueueBuilder->queueSubmissions();
    $this->output()->writeln(dt('@queue digest email(s) queued', ['@queue' => $queue_count]));
  }
}
