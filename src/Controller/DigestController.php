<?php

namespace Drupal\webform_digests\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Controller for sending webform digests.
 */
class DigestController {

  /**
   * The webform digests queue builder.
   *
   * @var \Drupal\webform_digests\WebformDigestsQueueBuilder
   */
  protected $webformDigestsQueueBuilder;

  /**
   * Constructs a new WebformDigestsCommands object.
   *
   * @param \Drupal\webform_digests\WebformDigestsQueueBuilder $webformDigestsQueueBuilder
   *   The webform digests queue builder.
   */
  public function __construct(WebformDigestsQueueBuilder $webformDigestsQueueBuilder) {
    $this->webformDigestsQueueBuilder = $webformDigestsQueueBuilder;
  }

  /**
   * Send the digests for the day.
   */
  public function sendAction() {
    $queue_count = $this->webformDigestsQueueBuilder->queueSubmissions();
    return new JsonResponse([
      'queued' => $queue_count,
    ]);
  }

}
